# README #

Python code used to analyse tracks from DeepLabCut. Work done for Kiehn Lab. 

### What is this repository for? ###

* Speed, Coordination analysis

### How do I get set up? ###

* Python 3.6
* python dlc.py

### Who do I talk to? ###

* raghav@di.ku.dk

### Thanks 
* [DeepLabCut](https://www.mousemotorlab.org/deeplabcut)

