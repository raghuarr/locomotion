import pandas as pd
import glob 
import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial.distance import pdist 
import pdb
from tools import videoMetadata
from filterpy.kalman import KalmanFilter
from filterpy.common import Q_discrete_white_noise
import argparse
import os
import seaborn as sns
from scipy.stats import norm
from scipy.signal import find_peaks
from coord import iqrMean
from matplotlib.gridspec import GridSpec

joints = ['hip','knee','ankle','foot']
colors = ['tab:blue','tab:orange','tab:green','tab:red']

def getSwingIdx(peaks,yval,thresh=0.02):
        
    ### Find out ymin in each cycle and pull it to ground 0
    C = len(peaks)
    for i in range(-1,C-1):
        if i == -1:
            lIdx = 0
        else:
            lIdx = peaks[i]
        uIdx = peaks[i+1]
        step = yval[lIdx:uIdx,:]
        yval[lIdx:uIdx,:] = step - step[:,-1].min()
    swing_idx = np.where((yval[:,-1] >= thresh) & (yval[:,-2] >= thresh))[0]
    return swing_idx, yval

def spatialSync(x,swing_idx):
#    pdb.set_trace()
    delta = 0.1
    x = x * dist.max(1).reshape(-1,1)/args.scale 
    x = x + np.arange(len(x))
    idx = np.where(np.diff(swing_idx) > 1)[0]
    idx = np.concatenate((swing_idx[[0]],idx))
    for i in idx[:-1]:

        tmp = x[swing_idx[i]:swing_idx[i+1]]
        tmp = tmp + np.arange(swing_idx[i+1]) * delta
#    delta = x[0,-1] - x[1,-1]
    stance_idx = np.delete(np.arange(len(swing_idx)),swing_idx)
    x[swing_idx] += x[swing_idx]*2 #delta
    x[stance_idx] += x[stance_idx]/2 #delta

    return x

def cycleAngles(peaks,angle,num_angles=51):
    C = len(peaks)
    cyc_angle = np.zeros((C-1,num_angles))

    for i in range(C-1):
        lIdx = peaks[i]
        uIdx = peaks[i+1]
        xAxis = np.linspace(lIdx,uIdx,num_angles)
        cyc_angle[i,:] = np.interp(xAxis,np.arange(lIdx,uIdx),angle[lIdx:uIdx])

    return cyc_angle

def pDist(arr):
    N = arr.shape[0]
    d = arr.shape[1]
    dist = np.zeros((N,int(d*(d-1)/2)))

    for i in range(N):
        dist[i] = pdist(arr[i].reshape(-1,1))

    return dist

def smooth(x,L=15):
    M = x.shape[1]
    xF = np.zeros((x.shape[0]-L+1,M))
    for i in range(x.shape[1]):
        xF[:,i] = np.convolve(x[:,i],np.ones((L,))/L,mode='valid')
    return xF

def makeVector(yval,xval,i,j):
    vx = (xval[:,i] - xval[:,j]).reshape(-1,1)
    vy = (yval[:,i] - yval[:,j]).reshape(-1,1)
    v = np.concatenate((vx,vy),axis=1)
    v = v/np.linalg.norm(v,axis=1).reshape(-1,1)
    return v

def measureAngles(yval,xval,i,j,k):
    v1 = makeVector(yval,xval,i,j)
    v2 = makeVector(yval,xval,k,j)
    angle = np.arccos((v1*v2).sum(1))
    return 180*angle/np.pi

def makeStickFigure(x,y,dist,dur,fName,cyc_angles,peaks,\
        swing_idx,time_sync,sticks_only,num_steps=10,skip=1):
    plt.clf()
    stance_col = 'tab:grey'
    swing_col = 'tab:red'
    t = np.linspace(0,dur,len(x))[::-1]
    if not sticks_only:
        fig = plt.figure(figsize=(20,10))
        gs = GridSpec(3,6,figure=fig,hspace=0.5,wspace=0.5)
    ### Plot instantaneous angles
        ax = fig.add_subplot(gs[1,:3])
        for i in range(len(joints)):    
            sns.lineplot(t,angles[i],label=joints[i])
        plt.ylim([-5,185])
        plt.xlim([-0.1,dur+1])
        plt.xlabel("Time")
        plt.legend()
        
        ### Plot angle densities
        ax = fig.add_subplot(gs[1,3:])
        norm=None
        B = 36
        for i in range(len(joints)):    
            sns.distplot(angles[i],bins=B,label=joints[i],fit=norm)
        plt.ylim([-0.01,0.11])
        plt.xticks(np.arange(0,190,30))
        ax.legend()
        plt.xlabel("Joint Angle in degrees")

        ### Plot cycle angles per joint
        idx = np.random.permutation(num_steps)
        cyc_angles = [cyc_angles[i][idx] for i in range(4)]
        for i in range(4):
            ax = fig.add_subplot(gs[2,i])
            mAng = cyc_angles[i].mean(0)
            sAng = cyc_angles[i].std(0)
            xAxis = np.linspace(0,1,len(mAng))
            plt.fill_between(xAxis,mAng-sAng,mAng+sAng,alpha=0.2,color=colors[i])
            plt.plot(xAxis,mAng,label=joints[i],color=colors[i])
            plt.ylim([-5,185])
            plt.title(joints[i])
        cyc_angles = np.array(cyc_angles)
        minAng = cyc_angles.min(2)
        maxAng = cyc_angles.max(2)
        diffAng = maxAng - minAng
        ax = fig.add_subplot(gs[2,i+1:])
        plt.boxplot(diffAng.T,showfliers=True)
        plt.xticks(np.arange(5),['']+joints)
        plt.title("Max-Min angle per cycle")
        plt.ylim([-10,130])
        ax = fig.add_subplot(gs[0,:])
    else:
        fig = plt.figure(figsize=(20,2))

    if time_sync:
         ### Plot sticks
        x = x * dist.max(1).reshape(-1,1)/args.scale + t.reshape(-1,1)
        x = x - (x[:,[-1]] - t.reshape(-1,1))
        plt.plot(x.T,y.T+0.1,'tab:grey',linewidth=0.25)
        plt.plot(x[swing_idx,:].T,y[swing_idx,:].T+0.1,'tab:red',linewidth=0.25)
        plt.stem(t,-0.05*np.ones(len(t)),'tab:grey',markerfmt=" ",basefmt=" ")
        plt.stem(t[swing_idx],-0.05*np.ones(len(swing_idx)),'tab:red',markerfmt=" ",basefmt=" ")
        plt.axis('off')
        plt.ylim([-1,1.1])
    else:
        # Pull tip to floor
        stance_idx = np.delete(np.arange(len(x)),swing_idx)
        y[stance_idx] = y[stance_idx] - y[stance_idx][:,[-1]]

        x = x * dist.max(1).reshape(-1,1)/args.scale
        step = 0
        delta=0.05
        for i in range(len(x)):
            if (swing_idx == i).sum():
                step += delta
                plt.plot(x[i]+step,y[i]+0.1,'tab:red',linewidth=0.25)
            else:
                step += 0.1*delta
                plt.plot(x[i]+step,y[i]+0.1,'tab:grey',linewidth=0.25)
        plt.axis('off')
        plt.ylim([-1,1.1])
#    t = np.linspace(0,dur,len(angles[0]))[::-1]
    plt.savefig(dest+fName.replace('.avi','.pdf'))

    return

##### MAIN ##########
parser = argparse.ArgumentParser()
parser.add_argument('--data', type=str, default='/home/raghav/erda/Roser_project/Tracking/',
                    help='Path to video files.')
parser.add_argument('--scale', type=int, default=8,
                    help='Scaling factor for distance')
parser.add_argument('--time_sync', action='store_true',default=False,
                    help='Synchronize sticks to temporally')
parser.add_argument('--sticks_only', action='store_true',default=False,
                    help='Plot stick figures only')

args = parser.parse_args()
np.random.seed(42)

model = 'DLC_resnet50_lateral_analysisOct26shuffle1_100000'
bodyparts = ['toe','foot','ankle','knee','hip','crest']
bodyparts = bodyparts[::-1]

data_dir = args.data
dest = data_dir+'stickFigs/'
if not os.path.exists(dest):
    os.mkdir(dest)

#'/home/raghav/erda/Roser_project/Tracking/LATERAL_videos/CNO_study_lateral/En1Cre_HoxB8FlpO_LoxFrt-Di/After_CNO/'
files = sorted(glob.glob(data_dir+'*.avi'))

files = [f.split('/')[-1] for f in files]
print("Found %d videos to process"%len(files))
M = len(bodyparts)
for fName in files:
    meta = videoMetadata(data_dir+fName)
    data_file = glob.glob(data_dir+'labels/'+fName.replace('.avi','*.h5'))[0]
    data = pd.read_hdf(data_file)
    N = data.shape[0]
    xval = np.zeros((N,M))
    yval = np.zeros((N,M))
    for m in range(M):
        xval[:,m] = data[model][bodyparts[m]]['x'].values
        yval[:,m] = data[model][bodyparts[m]]['y'].values
    # Smooth values
    xval = smooth(xval,L=10)
    yval = smooth(yval,L=10)

#    yval = 1-yval/yval.max()
    # Rescale x,y to be [0,1]; y per time point, x across
    yval = (1-(yval-yval.min()) / (yval.max()-yval.min()))
    xval = (xval - xval.min(1).reshape(-1,1)) / (xval.max(1)-xval.min(1)).reshape(-1,1)
    t = np.linspace(0,meta['dur'],len(xval))[::-1]
    dist = pDist(xval)
    dist = dist/dist.max()
    xAng = xval * dist.max(1).reshape(-1,1)/args.scale + t.reshape(-1,1)
    xAng = xAng - (xAng[:,[-1]] - t.reshape(-1,1))

    ### Measure cycles based on foot angle
#    peaks, _ = find_peaks(angles[-1])
    peaks, _ = find_peaks(yval[:,-1])
    meanStep = iqrMean(np.diff(peaks))
    ## Remove outlier peaks
#    peaks, _ = find_peaks(angles[-1],distance=meanStep)
    peaks, _ = find_peaks(yval[:,-1],distance=meanStep)

    swing_idx,yval = getSwingIdx(peaks,yval) 
    ### Measure angles
    angles = [measureAngles(yval,xAng,i,i+1,i+2) for i in range(4)]
    cyc_angles = [cycleAngles(peaks,angle) for angle in angles]
  
#    stance_idx = np.where(xval[:,-1] == 0)

    # Obtain pairwise distance
    

    ### Save cycle_angles
    np.save(dest+fName.replace('.avi','.npy'),cyc_angles)

    makeStickFigure(xval,yval,dist,meta['dur'],fName,cyc_angles,peaks,\
            swing_idx,time_sync=args.time_sync,sticks_only=args.sticks_only)

    
